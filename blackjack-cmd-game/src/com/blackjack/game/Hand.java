package com.blackjack.game;

import java.util.ArrayList;
import java.util.List;

import com.blackjack.deck.Card;
import com.blackjack.deck.Deck;
import com.blackjack.deck.Rank;

/**
 * This represents cards of player or dealer.
 * @author rgubba1
 *
 */
public class Hand {
	
    private List<Card> hand;
	private int handValue;
	
    public Hand(Deck deck) {
        hand = new ArrayList<Card>();
        for (int i = 0; i < 2; i++) {
            hand.add(deck.drawCard());
        }
        
    }
    
    public void addCard(Deck deck){
    	hand.add(deck.drawCard());
    }
    
    public int getScore(){
		int score = 0;
		for(int i=0; i<hand.size(); i++){
			score = score + hand.get(i).getValue();
		}
		
		if(score>21){
			for(int i=0; i<hand.size(); i++){
				if(hand.get(i).getValue()==Rank.ACE.getRank()){
					score = score - 11;
					score = score + 1;
				}
			}
		}
		return score;
	}
    
    public int getCardCount(){
    	return hand.size();
    }
 
    public boolean isBlackjack() {
        if (getScore() == 21) {
            System.out.println("Blackjack!");
            return true;
        }
        return false;
    }
 
    public boolean isCountOverflow() {
        if (handValue > 21) {
            return true;
        }
        return false;
    }
    
    public boolean canBeSplit(){
    	if(hand.get(0).getRank()==hand.get(1).getRank()){
    		return true;
    	}
    	return false;
    }
 
    public String toString() {
        return "Hand: " + hand + " \nHand value: " + getScore();
    }

}
