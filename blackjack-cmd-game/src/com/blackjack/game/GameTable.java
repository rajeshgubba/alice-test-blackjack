package com.blackjack.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.blackjack.deck.Deck;
import com.blackjack.players.Player;

/**
 * This is Blackjack GameTable.
 * It has players, a deck of cards, status of game, game cycle, bets, winners.
 * The same table can be used to play multiple games of blackjack.
 * @author rgubba1
 *
 */
public class GameTable {
	
	/*
	 * Actual Player
	 */
	private Player player;
	/*
	 * Computer Player (Dealer)
	 */
	private Player dealer;
	private Deck deck = new Deck();
	private boolean endGame = false;
	private int betAmount = 0;
	
	Scanner scan = new Scanner(System.in);
	
	/**
	 * Initialize GameTable with players
	 */
	public void initGame(){
		dealer = new Player("Dealer");
		System.out.println("Welcome! Enter Your Name:");
		String name = scan.nextLine();
		player = new Player(name);
		
	}
	
	/**
	 * Deal the initial cards to players
	 */
	private void dealInitialCards(){
		deck.initialize();
		deck.shuffle();
		player.setHand(deck);
		dealer.setHand(deck);
		//System.out.println("Initial cards dealt...");
		player.debitAmount(10);
		dealer.debitAmount(10);
		betAmount = 20;
	}
	
	/**
	 * The best possible Blackjack hand is an opening deal of an ace with any ten-point card.
	 */
	private boolean checkOpeningDeal() {
		if(player.getHand().getScore()==21){
			player.printHand();
			player.creditAmount(betAmount);
			betAmount = 0;
			displayWinner(player);
			endGame = true;
			return true;
		}
		if(dealer.getHand().getScore()==21){
			dealer.printHand();
			dealer.creditAmount(betAmount);
			betAmount = 0;
			displayWinner(dealer);
			endGame = true;
			return true;
		}
		
		return false;
	}

	/**
	 * Starts the game
	 */
	public void startGame(){
		endGame=false;
		dealInitialCards();
		boolean status = checkOpeningDeal();
		if(status){
			return;
		}
		
		while(!endGame){
			player.printHand();
			
			List<Integer> options = showOptions();
			
			int selectedOption = scan.nextInt();
			if(!options.contains(selectedOption)){
				selectedOption = -1;
			}
			// (1) Hit (2) Stand (3) Double Down (4) Split (5) Surrender
			switch(selectedOption){
			case 1:
				issueCardToPlayer();
				if(!endGame){
					playDealerTurn();
				}
				break;
			case 2:
				onClickStand(player);
				break;
			case 3:
				doubleDownThenEndGame();
				break;
			case 4:
				splitGame();
				break;
			case 5:
				surrenderGame();
				break;
			default:
				System.out.println("Invalid option. Try again!");
				break;
					
			}
			
		}
		
	}
	
	private void surrenderGame() {
		// TODO Auto-generated method stub
		
	}

	private void splitGame() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Double the player bet amount
	 * Issue one card to player
	 * End game
	 */
	private void doubleDownThenEndGame() {
		player.debitAmount(10);
		betAmount=betAmount+10;
		issueCardToPlayer();
		if(!endGame){
			onClickStand(player);
		}
	}

	/**
	 * Perform Stand operation
	 * Calculate score 
	 * End game
	 * @param p player who clicked stand
	 */
	private void onClickStand(Player p) {
		endGame = true;
		player.printHand();
		dealer.printHand();
		
		if(player.getHand().getScore() > dealer.getHand().getScore()){
			player.creditAmount(betAmount);
			betAmount = 0;
			displayWinner(player);
			return;
		} 
		if(player.getHand().getScore() < dealer.getHand().getScore()){
			dealer.creditAmount(betAmount);
			betAmount = 0;
			displayWinner(dealer);
			return;
		}
		
		if(player.getHand().getScore() == dealer.getHand().getScore()){
			System.out.println(">>>>>>>>>Match draw! Its Push!<<<<<<<<<<<");
			dealer.creditAmount(10);
			player.creditAmount(10);
			betAmount = 0;
			return;
		}
		
		if(p.getName().equals("Dealer")){
			
		}else{
			
		}
	}
	
	/**
	 * Utility to print winner
	 * @param winner
	 */
	public void displayWinner(Player winner){
		System.out.println("======================================");
		System.out.println(">>>>>>>>>>>>>>"+winner.getName()+" wins!<<<<<<<<<<<<");
		System.out.println("======================================");
		System.out.println("Dealer Balance: " +dealer.getWalletBalance() +"$");
		System.out.println("Player Balance: "+player.getWalletBalance() +"$");
	}
	
	/**
	 * Automated Dealer Game play
	 */
	private void playDealerTurn() {
		
		if(dealer.getHand().getScore() == 20 || dealer.getHand().getScore() == 19){
			onClickStand(dealer);
			return;
		}
		
		if(dealer.getHand().getScore() < 21){
			dealer.getHand().addCard(deck);
		}
		
		if(dealer.getHand().getScore() > 21) {
			endGame = true;
			player.printHand();
			dealer.printHand();
			player.creditAmount(betAmount);
			betAmount = 0;
			displayWinner(player);
			
			return;
		}

		if(dealer.getHand().isBlackjack()){
			endGame = true;
			player.printHand();
			dealer.printHand();
			dealer.creditAmount(betAmount);
			betAmount = 0;
			displayWinner(dealer);
			return;
		}
	}

	/**
	 * This is executed when players select Hit
	 */
	private void issueCardToPlayer() {
		player.getHand().addCard(deck);
		if(player.getHand().getScore() > 21){
			endGame = true;
			player.printHand();
			dealer.creditAmount(betAmount);
			betAmount = 0;
			displayWinner(dealer);
			
		}

		if(player.getHand().isBlackjack()){
			endGame = true;
			player.printHand();
			player.creditAmount(betAmount);
			betAmount = 0;
			displayWinner(player);
			
		}



	}

	/**
	 * Show possible options on each turn
	 */
	public List<Integer> showOptions(){
		System.out.println("Your bet: "+player.getBetAmount());
		List<Integer> options = new ArrayList<>();
		System.out.println("Select your option:");
		System.out.println("1. Hit");
		options.add(1);
		
		System.out.println("2. Stand");
		options.add(2);
		
		if(player.getCardCount()==2){
			System.out.println("3. Double Down");
			options.add(3);
		}
		if(player.getCardCount()==2 && player.eligibleForSplit()){
			System.out.println("4. Split");
			options.add(4);
		}
		if(player.getCardCount()==2){
			System.out.println("5. Surrender");
			options.add(5);
		}
		return options;
	}
	
	/**
	 * close the table
	 */
	public void close(){
		scan.close();
	}
	
	public boolean endGame(){
		return endGame;
	}

}
