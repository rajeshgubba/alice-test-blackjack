package com.blackjack.players;

import com.blackjack.deck.Deck;
import com.blackjack.game.Hand;

/**
 * Represents a player on Blackjack GameTable.
 * Player has hand, bet amount & wallet amount
 * @author rgubba1
 *
 */
public class Player {
	
	private String name;
	private Hand hand;
	private int walletAmount = 100;
	private int betAmount;
    
	public Player(String name) {
		this.name = name;
	}
	
	public void printHand(){
		System.out.println(name+"->"+hand.toString());
	}
	
	public Hand getHand(){
		return hand;
	}
	
	public String getName(){
		return name;
	}
	
	public int getCardCount(){
		return hand.getCardCount();
	}

	public boolean eligibleForSplit() {
		if(hand.getCardCount()==2 && hand.canBeSplit()){
			return true;
		}
		return false;
	}
	
	public void creditAmount(int wonAmount){
		walletAmount = walletAmount + wonAmount;
	}
	
	public void debitAmount(int lossAmount){
		betAmount = betAmount+lossAmount;
		walletAmount = walletAmount - lossAmount;
	}
	
	public int getWalletBalance(){
		return walletAmount;
	}
	
	public void setHand(Deck deck){
		betAmount = 0;
		hand = new Hand(deck);
	}
	
	public int getBetAmount(){
		return betAmount;
	}
	

}
