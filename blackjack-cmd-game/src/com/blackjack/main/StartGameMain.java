package com.blackjack.main;

import java.util.Scanner;

import com.blackjack.game.GameTable;

/**
 * Start of the game
 * @author rgubba1
 *
 */
public class StartGameMain {

	public static void main(String[] args) {
		
		System.out.println("Welcome!");
		GameTable t = new GameTable();
		t.initGame();
		Scanner scan = new Scanner(System.in);
		
		
		while(true){
			System.out.println("************************************");
			System.out.println("You want play new game ? (Y/N)");
			String input = scan.next();
			
			switch(input){
			case "Y":
			case "y":
					
					t.startGame();
					if(t.endGame()){
						System.out.println("Thank you! Play Again!!");
					}
					break;
			case "N":
			case "n":
					t.close();
					scan.close();
					System.out.println("Thank you! Bye!!");
					return;
			default:
				System.out.println("Invalid Input. Please enter again!");
				break;
			
			}
			
		}
		

	}

}
