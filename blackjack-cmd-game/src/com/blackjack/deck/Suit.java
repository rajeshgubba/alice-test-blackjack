package com.blackjack.deck;

/**
 * Enum for all suits
 * @author rgubba1
 *
 */
public enum Suit {
	
	SPADE("Spade"),
	DIAMOND("Diamond"),
	CLUB("Club"),
	HEART("Heart");
	
	private String name;
	
	private Suit(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
}
