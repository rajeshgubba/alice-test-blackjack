package com.blackjack.deck;
/**
 * Represents a card in Deck
 * @author rgubba1
 *
 */
public class Card {
	/*
	 * Rank of the card (A,1,2....J,Q,K)
	 */
	private Rank rank;
	/*
	 * 4 Suits
	 */
	private Suit suit;
	
	public Card(Rank r, Suit s) {
		this.rank = r;
		this.suit = s;
	}
	
	public Rank getRank() {
        return rank;
    }
 
    public int getValue() {
        return rank.getRank();
    }
 
    public String toString() {
        return String.format("%s-%s", rank, suit);
    }
    
	public void printCard(){
		System.out.print(rank.getRank()+suit.getName());
	}
}
