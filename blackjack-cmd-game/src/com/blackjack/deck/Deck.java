package com.blackjack.deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Deck is set of 52 Cards
 * @author rgubba1
 *
 */
public class Deck {

	private final List<Card> cards = new ArrayList<Card>(52);
	
	public Deck() {
		initialize();
	}
	
	public void initialize(){
		cards.clear();
		for(Rank r: Rank.values()){
			for(Suit s: Suit.values()){
				Card c = new Card(r,s);
				cards.add(c);
			}
		}
		shuffle();
	}
	
	public void shuffle(){
		Collections.shuffle(cards);
	}
	
	public Card drawCard(){
		return cards.remove(0);
	}
	
	public int getDeckSize(){
		return cards.size();
	}
}
